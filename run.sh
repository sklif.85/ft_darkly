#!/bin/bash

mkdir -r /tmp/iso-mount
mount -o Darkly_i386.iso /tmp/iso-mount
top_dir_mounts=()
for i in bin etc lib usr var
do
    top_dir_mounts+=(-v "/tmp/iso-mount/$i:/$i")
done
docker run --rm --read-only "${top_dir_mounts[@]}" --tmpfs /tmp --tmpfs /run busybox